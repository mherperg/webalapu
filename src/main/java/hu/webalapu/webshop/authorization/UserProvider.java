package hu.webalapu.webshop.authorization;

import hu.webalapu.webshop.entities.WebShopUser;
import hu.webalapu.webshop.repositories.UserRepository;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.io.Serializable;

/**
 * Created by herperger on 3/26/17.
 */
@SessionScoped
@ManagedBean(name = "userProvider", eager = true)
public class UserProvider implements Serializable {

    private WebShopUser currentUser;

    public WebShopUser getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(WebShopUser currentUser) {
        this.currentUser = currentUser;
    }
}
