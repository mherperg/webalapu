package hu.webalapu.webshop.authorization;


import hu.webalapu.webshop.entities.WebShopUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.regex.Pattern;

import static hu.webalapu.webshop.authorization.AccessControlPhaseListener.AccessLevel.*;

/**
 * Created by herperger on 3/26/17.
 */
public class AccessControlPhaseListener implements PhaseListener {

    private static final Logger log = LoggerFactory.getLogger(AccessControlPhaseListener.class);

    public static final String USER_PROVIDER = "userProvider";

    public enum AccessLevel {
        NONE, ADMIN, CUSTOMER
    }

    private final HashMap<AccessLevel, List<UrlFilter>> levelFilters = new HashMap<AccessLevel, List<UrlFilter>>();

    public AccessControlPhaseListener() {
        initLevels();

        requires(ADMIN)
                .include("/faces/admin/*");

        requires(CUSTOMER)
                .include("/faces/customer/*");
    }

    private void initLevels()
    {
        AccessLevel[] levels = AccessLevel.values();
        for (int i = 1; i < levels.length; i++)
        {
            levelFilters.put(levels[i], new ArrayList<UrlFilter>());
        }
    }

    private UrlFilter requires(AccessLevel level) {
        UrlFilter filter = new UrlFilter();
        List<UrlFilter> list = levelFilters.get(level);
        list.add(filter);
        return filter;
    }

    public void afterPhase(PhaseEvent event)
    {
        try
        {
            //check have correct access
            FacesContext context = event.getFacesContext();
            HttpSession session = (HttpSession) context.getExternalContext().getSession(true);


            UserProvider userProvider = (UserProvider) session.getAttribute(USER_PROVIDER);

            //can't use this here. only valid at render response phase?
            String viewId = context.getViewRoot().getViewId();

            AccessLevel required = requiredLevel(viewId);

            if(required == NONE) {
                return;
            }

            if(!"/faces/login.xhtml".equals(viewId) && userProvider == null) {
                redirectLogin(context);
            }

            log.debug("Required level={} for viewId={}", required, viewId);

            if (userProvider.getCurrentUser() != null) {
                //check if page require access:
                switch (required) {
                    case ADMIN:
                        if (userProvider.getCurrentUser().userType != WebShopUser.UserType.ADMIN)
                            redirectLogin(event.getFacesContext());
                        break;

                    case CUSTOMER:
                        if (userProvider.getCurrentUser().userType != WebShopUser.UserType.CUSTOMER)
                            redirectLogin(event.getFacesContext());
                        break;

                    default:
                        //error
                        log.error("huh?");
                        throw new IllegalArgumentException("Not a valid access level");
                }
            } else {
                redirectLogin(event.getFacesContext());
            }
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            log.error("beforePhase caught exception", e);
        }

    }

    private AccessLevel requiredLevel(String viewId)
    {
        AccessLevel[] levels = AccessLevel.values();
        for (int i = levels.length - 1; i > 0; i--)
        {
            if (checkLevel(levels[i], viewId))
                return levels[i];
        }

        return AccessLevel.NONE;
    }

    private boolean checkLevel(AccessLevel level, String viewId)
    {
        return matchUri(levelFilters.get(level), viewId);
    }

    private boolean matchUri(List<UrlFilter> list, String uri)
    {
        for (UrlFilter filter : list)
        {
            if (filter.matches(uri))
                return true;
        }
        return false;
    }

    private void redirectLogin(FacesContext context)
    {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        addError(context, "access.loginrequired");
        context.getApplication().getNavigationHandler().handleNavigation(context, null, "login");
    }

    private void addError(FacesContext context, String key)
    {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Authorization required", "You must be logged in.");
        FacesContext.getCurrentInstance().addMessage("login_error", message);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
    }

    public void beforePhase(PhaseEvent phaseEvent) {

    }

    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }

    private static class UrlFilter
    {
        private ArrayList<Pattern> include = new ArrayList<Pattern>();
        private ArrayList<Pattern> exclude = new ArrayList<Pattern>();

        public UrlFilter()
        {

        }

        /**
         * Include the wildcard(*) built pattern.
         *
         * @param pattern
         * @return
         */
        public UrlFilter include(String pattern)
        {
            include.add(generateExpression(pattern));
            return this;
        }

        /**
         * Exclude the wildcard(*) built pattern.
         *
         * @param pattern
         * @return
         */
        public UrlFilter exclude(String pattern)
        {
            exclude.add(generateExpression(pattern));
            return this;
        }

        /**
         * Checks to see if uri matches at least ONE inclusion filter and doesn't match ANY exclusion filters.
         *
         * @param uri
         * @return
         */
        public boolean matches(String uri)
        {
            boolean match = false;

            //check inclusions
            for (Pattern pattern : include)
            {
                match = match || pattern.matcher(uri).matches();
            }

            if (!match)
                return false;

            //check exclusions
            for (Pattern pattern : exclude)
            {
                match = match && !pattern.matcher(uri).matches();
            }
            return match;
        }

        /** regular expression special character */
        private static char[] specialChars = { '[', '\\', '^', '$', '.', '|', '?', '*', '+', '(', ')' };

        /**
         *
         * @param input
         * @return
         */
        private static Pattern generateExpression(String input)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < input.length(); i++)
            {
                char letter = input.charAt(i);
                if (letter == '*')
                {
                    sb.append(".*");
                }
                else if (contains(specialChars, letter))
                {
                    sb.append("\\" + letter);
                }
                else
                {
                    sb.append(letter);
                }
            }
            return Pattern.compile(sb.toString());
        }

        private static boolean contains(char[] array, char value)
        {
            if (array == null || array.length == 0)
            {
                return false;
            }

            for (int i = 0; i < array.length; i++)
            {
                char o = array[i];
                if (o == value)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
