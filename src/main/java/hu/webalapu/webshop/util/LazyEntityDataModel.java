package hu.webalapu.webshop.util;

import hu.webalapu.webshop.entities.BaseEntity;
import hu.webalapu.webshop.repositories.BaseRepository;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import java.util.List;
import java.util.Map;

/**
 * Created by herperger on 4/22/17.
 */
public class LazyEntityDataModel<T extends BaseEntity> extends LazyDataModel<T> {
    private BaseRepository<T> entityRepository;
    private int rowIndex;

    private List<T> entityList;

    public LazyEntityDataModel(BaseRepository<T> entityRepository) {
        this.entityRepository = entityRepository;
    }

    @Override
    public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        entityList = entityRepository.findPaginated(first, pageSize, sortField, sortOrder == SortOrder.ASCENDING, filters);

        this.setRowCount((int) entityRepository.count());

        return entityList;
    }

    @Override
    public boolean isRowAvailable() {
        if(entityList == null)
            return false;
        int index = rowIndex % getPageSize() ;
        return index >= 0 && index < entityList.size();
    }

    @Override
    public Object getRowKey(T object) {
        return object.getId().toString();
    }

    @Override
    public T getRowData() {
        return entityList.get(rowIndex % getPageSize());
    }

    @Override
    public int getRowIndex() {
        return this.rowIndex;
    }

    @Override
    public T getRowData(String rowKey) {
        if(entityList == null) {
            return null;
        }

        return entityList.stream().filter(product -> product.getId().toString().equals(rowKey))
                .findFirst().orElse(null);
    }

    /**
     * Sets row index
     * @param rowIndex
     */
    @Override
    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    @Override
    public Object getWrappedData() {
        return entityList;
    }

    @Override
    public void setWrappedData(Object list) {
        this.entityList = (List<T>) list;
    }
}
