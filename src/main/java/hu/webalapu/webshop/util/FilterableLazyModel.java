package hu.webalapu.webshop.util;

import hu.webalapu.webshop.entities.BaseEntity;
import hu.webalapu.webshop.repositories.BaseRepository;
import org.primefaces.model.SortOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by herperger on 5/13/17.
 */
public class FilterableLazyModel<T extends BaseEntity> extends LazyEntityDataModel<T> {

    private Map<String, Object> filters;

    public FilterableLazyModel(BaseRepository<T> entityRepository) {
        super(entityRepository);
        filters = new HashMap<>();
    }

    @Override
    public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        return super.load(first, pageSize, sortField, sortOrder, this.filters);
    }

    public void addFilter(String field, Object value) {
        filters.put(field, value);
    }
}
