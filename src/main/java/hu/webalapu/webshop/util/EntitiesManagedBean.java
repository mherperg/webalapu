package hu.webalapu.webshop.util;

import hu.webalapu.webshop.entities.BaseEntity;
import hu.webalapu.webshop.repositories.BaseRepository;
import org.primefaces.model.LazyDataModel;

import javax.faces.event.ActionEvent;
import java.util.List;

/**
 * Created by herperger on 4/22/17.
 */
public class EntitiesManagedBean<T extends BaseEntity> {

    private List<T> selectedEntities;

    // Lazy loading list
    private LazyDataModel<T> lazyModel;

    // Selected user that will be updated
    private T editedEntity;

    private Class<T> entityClass;
    private BaseRepository<T> entityRepository;

    public void init(Class<T> entityClass, BaseRepository<T> entityRepository) {
        this.entityClass = entityClass;
        this.entityRepository = entityRepository;
        lazyModel = createLazyModel(entityRepository);

        try {
            editedEntity = entityClass.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    protected LazyEntityDataModel<T> createLazyModel(BaseRepository<T> entityRepository) {
        return new LazyEntityDataModel<T>(entityRepository);
    }

    /**
     * Create, Update and Delete operations
     */
    public void doSaveEntity() {
        entityRepository.save(editedEntity);

        try {
            editedEntity = entityClass.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *
     * @param actionEvent
     */
    public void doDeleteEntities(ActionEvent actionEvent){
        selectedEntities.forEach(entity -> entityRepository.remove(entity));
    }

    public List<T> getSelectedEntities() {
        return selectedEntities;
    }

    public void setSelectedEntities(List<T> selectedEntities) {
        this.selectedEntities = selectedEntities;
    }

    public LazyDataModel<T> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<T> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public T getEditedEntity() {
        return editedEntity;
    }

    public void prepareNewEntity() {
        try {
            editedEntity = entityClass.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public void setEditedEntity(T editedEntity) {
        this.editedEntity = editedEntity;
    }
}
