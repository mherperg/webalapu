package hu.webalapu.webshop.controllers.main;

import hu.webalapu.webshop.authorization.UserProvider;
import hu.webalapu.webshop.entities.WebShopUser;
import hu.webalapu.webshop.entities.etc.Address;
import hu.webalapu.webshop.repositories.UserRepository;
import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Krisztian on 2017. 05. 13..
 */
@ManagedBean(name = "registration")
@RequestScoped
public class RegistrationBean {
    @Inject
    private UserRepository userRepository;

    private String password;
    private String password2;
    private String username;
    private String fullname;
    private String email;
    private String email2;
    private String phone;
    private String city;
    private String address;
    private String country;
    private String birthDate;
    private boolean acceptTerms;

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }
    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getFullname() {
        return fullname;
    }
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail2() {
        return email2;
    }
    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAcceptTerms(boolean acceptTerms) {
        this.acceptTerms = acceptTerms;
    }
    public boolean isAcceptTerms() {
        return acceptTerms;
    }
    public void addMessage() {
        String summary = acceptTerms ? "Checked" : "Unchecked";
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(summary));
    }

    public String register() {
        RequestContext context = RequestContext.getCurrentInstance();

        FacesMessage message;
        String toReturn;

        WebShopUser user1 = new WebShopUser();
        user1.username = username;
        user1.password = password;
        user1.userType = WebShopUser.UserType.CUSTOMER;
        user1.fullname = fullname;
        user1.email = email;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = dateFormat.parse(birthDate);
            user1.dateOfBirth = date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        user1.phone = "1234556";
        user1.billingAddress = new Address();
        user1.billingAddress.address = address;
        user1.billingAddress.city = city;
        user1.billingAddress.country = country;
        userRepository.save(user1);

        message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registration success", "");
        FacesContext.getCurrentInstance().addMessage("registration_success", message);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

        return redirectLogin();
    }

    private String redirectLogin() {
        FacesMessage message;
        String toReturn;
        toReturn = "login.xhtml?faces-redirect=true";
        return toReturn;
    }
    private String redirectRegistration(){
        String toReturn = "registration.xhtml?faces-redirect=true";
        return toReturn;
    }

}
