package hu.webalapu.webshop.controllers.main;

import hu.webalapu.webshop.authorization.UserProvider;
import hu.webalapu.webshop.entities.WebShopUser;
import hu.webalapu.webshop.repositories.UserRepository;
import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 * Created by herperger on 3/26/17.
 */
@ManagedBean(name = "login")
@RequestScoped
public class LoginBean {

    @ManagedProperty(value="#{userProvider}")
    private UserProvider userProvider;

    @Inject
    private UserRepository userRepository;

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String login() {
        String toReturn;
        if(username != null && password != null) {
            WebShopUser webShopUser = userRepository.findByUsername(username);

            if (webShopUser == null || !password.equals(webShopUser.password)) {
                toReturn = redirectLogin();
            } else {
                userProvider.setCurrentUser(webShopUser);
                switch (webShopUser.userType) {
                    case ADMIN:
                        toReturn = "admin/crud/productcategories/productcategories?faces-redirect=true";
                        break;
                    case CUSTOMER:
                        toReturn = "customer/customerpage?faces-redirect=true";
                        break;
                    default:
                        toReturn = null;
                        break;
                }
            }
        } else {
            toReturn = redirectLogin();
        }
        return toReturn;
    }

    private String redirectLogin() {
        FacesMessage message;
        String toReturn;
        message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login Error", "Invalid credentials");
        FacesContext.getCurrentInstance().addMessage("login_error", message);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        toReturn = "login.xhtml?faces-redirect=true";
        return toReturn;
    }

    public String redirectRegistration() {
        String toReturn;
        toReturn = "registration.xhtml";
        return toReturn;
    }


    public void setUserProvider(UserProvider userProvider) {
        this.userProvider = userProvider;
    }
}
