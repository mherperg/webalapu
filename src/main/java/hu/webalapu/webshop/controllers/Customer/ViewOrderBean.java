package hu.webalapu.webshop.controllers.Customer;

import hu.webalapu.webshop.entities.ProductOrder;
import hu.webalapu.webshop.entities.WebShopUser;
import hu.webalapu.webshop.repositories.OrderRepository;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

/**
 * Created by herperger on 5/14/17.
 */
@ManagedBean
@RequestScoped
public class ViewOrderBean {
    @ManagedProperty(value = "#{param.orderIdentifier}")
    private String orderIdentifier;

    @Inject
    private OrderRepository orderRepository;

    public ProductOrder getOrder() {
        return orderRepository.findByIdentifier(orderIdentifier);
    }

    public String getOrderIdentifier() {
        return orderIdentifier;
    }

    public void setOrderIdentifier(String orderIdentifier) {
        this.orderIdentifier = orderIdentifier;
    }
}
