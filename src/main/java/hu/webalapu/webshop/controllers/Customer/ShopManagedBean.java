package hu.webalapu.webshop.controllers.Customer;

import hu.webalapu.webshop.entities.Product;
import hu.webalapu.webshop.entities.ProductCategory;
import hu.webalapu.webshop.repositories.BaseRepository;
import hu.webalapu.webshop.repositories.ProductCategoryRepository;
import hu.webalapu.webshop.repositories.ProductInstanceRepository;
import hu.webalapu.webshop.repositories.ProductRepository;
import hu.webalapu.webshop.util.EntitiesManagedBean;
import hu.webalapu.webshop.util.FilterableLazyModel;
import hu.webalapu.webshop.util.LazyEntityDataModel;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by herperger on 5/13/17.
 */
@ManagedBean(name = "shop")
@SessionScoped
public class ShopManagedBean extends EntitiesManagedBean<Product> {

    @Inject
    private ProductRepository productRepository;

    @Inject
    private ProductCategoryRepository productCategoryRepository;

    private ProductCategory selectedProductCategory;

    private FilterableLazyModel lazyModel;

    @PostConstruct
    public void init() {
        super.init(Product.class, productRepository);
    }

    @Override
    protected LazyEntityDataModel<Product> createLazyModel(BaseRepository<Product> entityRepository) {
        return new FilterableLazyModel<>(productRepository);
    }

    public List<ProductCategory> getProductCategories() {
        return productCategoryRepository.findAll();
    }

    public void setSelectProductCategory(ProductCategory productCategory) {
        ((FilterableLazyModel<Product>) getLazyModel()).addFilter("productCategory", productCategory);
    }

    public long getStockCount(Product product) {
        return productRepository.findStockCount(product);
    }

}
