package hu.webalapu.webshop.controllers.Customer;

import hu.webalapu.webshop.entities.CartItem;
import hu.webalapu.webshop.entities.Product;
import hu.webalapu.webshop.entities.ProductOrder;
import hu.webalapu.webshop.entities.WebShopUser;
import hu.webalapu.webshop.services.OrderService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by hory9 on 2017.05.13..
 */
@ManagedBean(name = "cart")
@SessionScoped
public class CartManagedBean {

    @Inject
    private OrderService orderService;

    @ManagedProperty(value = "#{confirmOrder}")
    private ConfirmOrderManagedBean confirmOrder;

    private List<CartItem> cartItems = new ArrayList<>();

    public void addProduct(Product product) {
        Optional<CartItem> optional = cartItems.stream().filter(cartItem -> cartItem.getProduct() == product).findAny();
        if(optional.isPresent()) {
            CartItem found = optional.get();
            found.increaseQuantity();
        } else {
            this.cartItems.add(new CartItem(product));
        }
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public String confirmOrder() {
        ProductOrder newOrder = orderService.makeOrder(cartItems, null);
        cartItems.clear();
        if(newOrder != null) {
            confirmOrder.setLastOrder(newOrder);

            return "/faces/guest/confirmGuestOrder?faces-redirect=true";
        } else {
            return "";
        }
    }

    public String confirmCustomerOrder(WebShopUser user) {
        ProductOrder newOrder = orderService.makeOrder(cartItems, user);
        cartItems.clear();
        if(newOrder != null) {
            confirmOrder.setLastOrder(newOrder);

            return "/faces/guest/confirmCustomerOrder?faces-redirect=true";
        } else {
            return "";
        }
    }

    public ConfirmOrderManagedBean getConfirmOrder() {
        return confirmOrder;
    }

    public void setConfirmOrder(ConfirmOrderManagedBean confirmOrder) {
        this.confirmOrder = confirmOrder;
    }
}
