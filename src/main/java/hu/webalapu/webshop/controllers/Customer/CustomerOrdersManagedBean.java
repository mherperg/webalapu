package hu.webalapu.webshop.controllers.Customer;

import hu.webalapu.webshop.authorization.UserProvider;
import hu.webalapu.webshop.entities.ProductOrder;
import hu.webalapu.webshop.repositories.OrderRepository;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by herperger on 5/14/17.
 */
@ManagedBean
@RequestScoped
public class CustomerOrdersManagedBean {
    @Inject
    private OrderRepository orderRepository;

    @ManagedProperty(value="#{userProvider}")
    private UserProvider userProvider;

    @ManagedProperty(value="#{viewCustomerOrder}")
    private ViewCustomerOrderBean viewCustomerOrderBean;

    public List<ProductOrder> getCustomerOrders() {
        return orderRepository.findByUser(userProvider.getCurrentUser());
    }

    public String viewProductOrder(ProductOrder productOrder) {
        viewCustomerOrderBean.setOrder(productOrder);

        return "/faces/customer/viewCustomerOrder?faces-redirect=true";
    }

    public UserProvider getUserProvider() {
        return userProvider;
    }

    public void setUserProvider(UserProvider userProvider) {
        this.userProvider = userProvider;
    }

    public ViewCustomerOrderBean getViewCustomerOrderBean() {
        return viewCustomerOrderBean;
    }

    public void setViewCustomerOrderBean(ViewCustomerOrderBean viewCustomerOrderBean) {
        this.viewCustomerOrderBean = viewCustomerOrderBean;
    }
}
