package hu.webalapu.webshop.controllers.Customer;

import hu.webalapu.webshop.authorization.UserProvider;
import hu.webalapu.webshop.entities.CartItem;
import hu.webalapu.webshop.entities.Product;
import hu.webalapu.webshop.entities.ProductOrder;
import hu.webalapu.webshop.entities.WebShopUser;
import hu.webalapu.webshop.services.OrderService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by herperger on 5/14/17.
 */
@ManagedBean(name = "customerCart")
@SessionScoped
public class CustomerCartManagedBean {
    @Inject
    private OrderService orderService;

    @ManagedProperty(value="#{userProvider}")
    private UserProvider userProvider;

    private List<CartItem> cartItems = new ArrayList<>();

    public void addProduct(Product product) {
        Optional<CartItem> optional = cartItems.stream().filter(cartItem -> cartItem.getProduct() == product).findAny();
        if(optional.isPresent()) {
            CartItem found = optional.get();
            found.increaseQuantity();
        } else {
            this.cartItems.add(new CartItem(product));
        }
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public String confirmOrder() {
        ProductOrder newOrder = orderService.makeOrder(cartItems, userProvider.getCurrentUser());
        cartItems.clear();
        if(newOrder != null) {
            return "/faces/customer/customerOrders?faces-redirect=true";
        } else {
            return "";
        }
    }

    public UserProvider getUserProvider() {
        return userProvider;
    }

    public void setUserProvider(UserProvider userProvider) {
        this.userProvider = userProvider;
    }
}
