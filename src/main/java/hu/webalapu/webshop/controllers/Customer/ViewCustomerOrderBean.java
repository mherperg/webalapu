package hu.webalapu.webshop.controllers.Customer;

import hu.webalapu.webshop.entities.ProductOrder;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * Created by herperger on 5/14/17.
 */
@SessionScoped
@ManagedBean(name = "viewCustomerOrder")
public class ViewCustomerOrderBean {
    private ProductOrder order;

    public ProductOrder getOrder() {
        return order;
    }

    public void setOrder(ProductOrder order) {
        this.order = order;
    }
}
