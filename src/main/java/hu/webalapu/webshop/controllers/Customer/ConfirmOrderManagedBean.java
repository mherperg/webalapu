package hu.webalapu.webshop.controllers.Customer;

import hu.webalapu.webshop.entities.ProductOrder;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by herperger on 5/14/17.
 */
@ManagedBean(name = "confirmOrder")
@SessionScoped
public class ConfirmOrderManagedBean {
    private ProductOrder lastOrder;

    public String getOrderLink() {

        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String url = request.getRequestURL().toString();
        String baseURL = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath() + "/";

        return baseURL + "faces/guest/viewGuestOrder.xhtml?orderIdentifier=" + lastOrder.getIdentifier();
    }

    public void setLastOrder(ProductOrder lastOrder) {
        this.lastOrder = lastOrder;
    }
}
