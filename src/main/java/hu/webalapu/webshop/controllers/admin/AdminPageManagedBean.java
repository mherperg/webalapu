package hu.webalapu.webshop.controllers.admin;

import hu.webalapu.webshop.authorization.UserProvider;
import hu.webalapu.webshop.entities.WebShopUser;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 * Created by herperger on 3/26/17.
 */
@ManagedBean(name = "admin")
@RequestScoped
public class AdminPageManagedBean {
    @ManagedProperty(value="#{userProvider}")
    UserProvider userProvider;

    public String getUserName() {
        WebShopUser currentUser = userProvider.getCurrentUser();
        if(currentUser == null) {
            return null;
        }
        return currentUser.username;
    }

    public void setUserProvider(UserProvider userProvider) {
        this.userProvider = userProvider;
    }

    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "login";
    }
}
