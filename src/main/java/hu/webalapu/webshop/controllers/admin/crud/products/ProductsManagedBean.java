package hu.webalapu.webshop.controllers.admin.crud.products;

import hu.webalapu.webshop.entities.ProductCategory;
import hu.webalapu.webshop.util.EntitiesManagedBean;
import hu.webalapu.webshop.entities.Product;
import hu.webalapu.webshop.repositories.ProductCategoryRepository;
import hu.webalapu.webshop.repositories.ProductRepository;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;


/**
 * Created by herperger on 4/21/17.
 */
@ManagedBean(name = "products")
@SessionScoped
public class ProductsManagedBean extends EntitiesManagedBean<Product> {

    @Inject
    private ProductRepository productRepository;

    @Inject
    private ProductCategoryRepository productCategoryRepository;

    @PostConstruct
    public void init() {
        super.init(Product.class, productRepository);
    }

    public List<ProductCategory> getProductCategories() {
        return productCategoryRepository.findAll();
    }

    @ManagedBean(name = "productCategoryConverter")
    @ApplicationScoped
    public static class ProductCategoryConverter implements Converter {

        @Inject
        private ProductCategoryRepository productCategoryRepository;

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            return productCategoryRepository.findById(Long.parseLong(value));
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            return ((ProductCategory)value).getId().toString();
        }
    }
}
