package hu.webalapu.webshop.controllers.admin.crud;

import hu.webalapu.webshop.entities.ProductOrder;
import hu.webalapu.webshop.entities.ProductCategory;
import hu.webalapu.webshop.repositories.OrderRepository;
import hu.webalapu.webshop.util.EntitiesManagedBean;
import hu.webalapu.webshop.repositories.ProductCategoryRepository;
import hu.webalapu.webshop.repositories.ProductRepository;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by hory9 on 2017.05.07..
 */
@ManagedBean(name = "orders")
@SessionScoped
public class OrdersManagedBean extends EntitiesManagedBean<ProductOrder>{
    @Inject
    private OrderRepository orderRepository;

    @PostConstruct
    public void  init(){
        super.init(ProductOrder.class, orderRepository);
    }

    public void markAsComplete(){
        getEditedEntity().setOrderStatus(ProductOrder.OrderStatus.COMPLETED);
        doSaveEntity();
    }
}
