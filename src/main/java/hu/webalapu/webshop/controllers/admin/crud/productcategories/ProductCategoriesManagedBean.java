package hu.webalapu.webshop.controllers.admin.crud.productcategories;

import hu.webalapu.webshop.util.EntitiesManagedBean;
import hu.webalapu.webshop.entities.ProductCategory;
import hu.webalapu.webshop.repositories.ProductCategoryRepository;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

/**
 * Created by herperger on 4/29/17.
 */
@ManagedBean(name = "productCategories")
@SessionScoped
public class ProductCategoriesManagedBean extends EntitiesManagedBean<ProductCategory>{
    @Inject
    private ProductCategoryRepository productCategoryRepository;

    @PostConstruct
    public void init() {
        super.init(ProductCategory.class, productCategoryRepository);
    }
}
