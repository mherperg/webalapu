package hu.webalapu.webshop.controllers.inventorymanagement;

import hu.webalapu.webshop.entities.Product;
import hu.webalapu.webshop.entities.ProductCategory;
import hu.webalapu.webshop.entities.ProductInstance;
import hu.webalapu.webshop.repositories.ProductCategoryRepository;
import hu.webalapu.webshop.repositories.ProductInstanceRepository;
import hu.webalapu.webshop.repositories.ProductRepository;
import hu.webalapu.webshop.util.EntitiesManagedBean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by herperger on 5/1/17.
 */
@ManagedBean(name = "productInstances")
@SessionScoped
public class InventoryManagementManagedBean extends EntitiesManagedBean<ProductInstance> {
    @Inject
    private ProductInstanceRepository productInstanceRepository;

    @Inject
    private ProductRepository productRepository;

    @PostConstruct
    public void init() {
        super.init(ProductInstance.class, productInstanceRepository);
    }

    public List<Product> getProducts() {
        return productRepository.findAll();
    }


    @ManagedBean(name = "productConverter")
    @ApplicationScoped
    public static class ProductCategoryConverter implements Converter {

        @Inject
        private ProductRepository productRepository;

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String value) {
            return productRepository.findById(Long.parseLong(value));
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            return ((Product)value).getId().toString();
        }
    }
}
