package hu.webalapu.webshop.entities.etc;

import hu.webalapu.webshop.entities.BaseEntity;

import javax.persistence.Embeddable;
import javax.persistence.Entity;

/**
 * Created by Krisztian on 2017. 05. 13..
 */
@Embeddable
public class Address {

    public String city;
    public String address;
    public String country;

    public Address(){}

    public Address(String city, String address, String country){
        this.city = city;
        this.address = address;
        this.country = country;
    }

}
