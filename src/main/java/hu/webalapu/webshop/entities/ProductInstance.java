package hu.webalapu.webshop.entities;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by herperger on 4/29/17.
 */
@Entity
public class ProductInstance extends BaseEntity {

    public enum ProductInstanceState {
        IN_STOCK, OUT_OF_STOCK;
    }

    @ManyToOne
    @NotNull
    private Product product;

    private Date stockDate = new Date();
    private String serialNumber;

    @Convert(converter = ProductInstanceStateConverter.class)
    private ProductInstanceState productInstanceState = ProductInstanceState.IN_STOCK;

    public ProductInstance() {
    }

    public Date getStockDate() {
        return stockDate;
    }

    public void setStockDate(Date stockDate) {
        this.stockDate = stockDate;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setOutOfStock() {
        this.productInstanceState = ProductInstanceState.OUT_OF_STOCK;
    }

    public static class ProductInstanceStateConverter implements AttributeConverter<ProductInstanceState, String> {
        public String convertToDatabaseColumn(ProductInstanceState attribute) {
            return attribute.name();
        }

        public ProductInstanceState convertToEntityAttribute(String dbData) {
            return ProductInstanceState.valueOf(dbData);
        }
    }
}
