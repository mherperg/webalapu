package hu.webalapu.webshop.entities;

import com.sun.istack.internal.Nullable;
import hu.webalapu.webshop.entities.etc.Address;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by herperger on 3/26/17.
 */
@Entity
public class WebShopUser extends BaseEntity {

    @Convert(converter = UserTypeConverter.class)
    public UserType userType;
    public enum UserType { ADMIN, CUSTOMER, STOREKEEPER }

    public String username;
    public String password;

    public String fullname;
    public String email;
    public String phone;

    @Embedded
    @Nullable
    public Address billingAddress = new Address();
    public Date dateOfBirth;

    //Getters
    public String getUsername(){return username;}

    //Enum converters
    public static class UserTypeConverter implements AttributeConverter<UserType, String> {
        public String convertToDatabaseColumn(UserType attribute) {
            return attribute.name();
        }
        public UserType convertToEntityAttribute(String dbData) {
            return UserType.valueOf(dbData);
        }
    }

}
