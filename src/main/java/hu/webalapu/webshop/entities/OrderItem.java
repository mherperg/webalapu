package hu.webalapu.webshop.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by herperger on 4/29/17.
 */
@Entity
public class OrderItem extends BaseEntity {

    @ManyToMany(fetch = FetchType.EAGER)
    private List<ProductInstance> productInstances = new ArrayList<>();

    @ManyToOne
    private ProductOrder order;

    @ManyToOne
    private Product product;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return productInstances.size();
    }

    public OrderItem(ProductOrder order) {
        this.order = order;
    }

    public OrderItem() {
    }

    public void addProductInstance(ProductInstance productInstance) {
        this.productInstances.add(productInstance);
    }

    public ProductOrder getOrder() {
        return order;
    }

    public void setOrder(ProductOrder order) {
        this.order = order;
    }

    public int calculateTotal() {
        int total = 0;

        for (ProductInstance productInstance :
                productInstances) {
            total += productInstance.getProduct().getPrice();
        }

        return total;
    }
}
