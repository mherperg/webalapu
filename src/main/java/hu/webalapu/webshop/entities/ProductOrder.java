package hu.webalapu.webshop.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by herperger on 4/29/17.
 */
@Entity
public class ProductOrder extends BaseEntity {
    public enum OrderStatus {
        NEW, COMPLETED;
    }

    @Convert(converter = OrderStatusConverter.class)
    private OrderStatus orderStatus = OrderStatus.NEW;

    @OneToMany(mappedBy = "order", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    public List<OrderItem> orderItems = new ArrayList<>();

    @ManyToOne
    private WebShopUser webShopUser;

    private String identifier;

    private Date orderDate = new Date();

    public WebShopUser getWebShopUser() {
        return webShopUser;
    }

    public void setWebShopUser(WebShopUser webShopUser) {
        this.webShopUser = webShopUser;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void addOrderItem(OrderItem orderItem) {
        this.orderItems.add(orderItem);
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int calculateTotal() {
        int total = 0;

        for (OrderItem orderItem: orderItems) {
            total += orderItem.calculateTotal();
        }
        return total;
    }

    public static class OrderStatusConverter implements AttributeConverter<OrderStatus, String> {
        public String convertToDatabaseColumn(OrderStatus attribute) {
            return attribute.name();
        }

        public OrderStatus convertToEntityAttribute(String dbData) {
            return OrderStatus.valueOf(dbData);
        }
    }
}
