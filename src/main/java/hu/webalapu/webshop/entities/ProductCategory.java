package hu.webalapu.webshop.entities;

import javax.persistence.Entity;

/**
 * Created by herperger on 4/29/17.
 */

@Entity
public class ProductCategory extends BaseEntity {
    private String name;

    public ProductCategory(String name) {
        this.name = name;
    }

    public ProductCategory() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ProductCategory && o != null && ((ProductCategory) o).getId().equals(id);
    }
}
