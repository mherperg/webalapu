package hu.webalapu.webshop.repositories;

import hu.webalapu.webshop.entities.Product;
import hu.webalapu.webshop.entities.ProductInstance;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import java.util.List;

/**
 * Created by herperger on 5/1/17.
 */
@Stateless
public class ProductInstanceRepository extends BaseRepository<ProductInstance> {

    @Inject
    public ProductInstanceRepository() {
        super(ProductInstance.class);
    }

    public List<ProductInstance> findByProduct(Product product, int count) {
        return (List<ProductInstance>) em.createQuery("SELECT pi FROM ProductInstance pi where pi.product.id = ?1 AND pi.productInstanceState = ?2" +
                " ORDER BY pi.stockDate")
                .setParameter(1, product.getId())
                .setParameter(2, ProductInstance.ProductInstanceState.IN_STOCK)
                .setMaxResults(count)
                .getResultList();
    }
}
