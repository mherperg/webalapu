package hu.webalapu.webshop.repositories;

import hu.webalapu.webshop.entities.WebShopUser;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;


/**
 * Created by herperger on 3/26/17.
 */
@Stateless
public class UserRepository extends BaseRepository<WebShopUser> {

    @Inject
    public UserRepository() {
        super(WebShopUser.class);
    }

    public WebShopUser findByUsername(String username) {
        try {
            return (WebShopUser) em.createQuery("SELECT u FROM WebShopUser u WHERE u.username = ?1").setParameter(1, username).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
