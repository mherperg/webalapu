package hu.webalapu.webshop.repositories;

import hu.webalapu.webshop.entities.ProductCategory;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * Created by herperger on 4/29/17.
 */
@Stateless
public class ProductCategoryRepository extends BaseRepository<ProductCategory> {

    @Inject
    public ProductCategoryRepository() {
        super(ProductCategory.class);
    }
}
