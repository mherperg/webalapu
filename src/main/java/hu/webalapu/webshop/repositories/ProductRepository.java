package hu.webalapu.webshop.repositories;

import hu.webalapu.webshop.entities.Product;
import hu.webalapu.webshop.entities.ProductInstance;

import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 * Created by herperger on 4/21/17.
 */

@Stateless
public class ProductRepository extends BaseRepository<Product> {

    @Inject
    public ProductRepository() {
        super(Product.class);
    }

    public long findStockCount(Product product) {
        return (long) em.createQuery("select count(pi) from ProductInstance pi where pi.product.id = ?1 and pi.productInstanceState = ?2")
                .setParameter(1, product.getId())
                .setParameter(2, ProductInstance.ProductInstanceState.IN_STOCK)
                .getSingleResult();
    }
}
