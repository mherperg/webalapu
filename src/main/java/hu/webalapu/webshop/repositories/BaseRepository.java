package hu.webalapu.webshop.repositories;

import hu.webalapu.webshop.entities.BaseEntity;
import org.hibernate.criterion.Order;

import javax.persistence.*;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

/**
 * Created by herperger on 2/21/17.
 */
public class BaseRepository<T extends BaseEntity> {

    @PersistenceContext
    protected EntityManager em;

    protected Class<T> entityClass;

    public BaseRepository(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public List<T> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(entityClass);
        Root<T> rootEntry = cq.from(entityClass);
        CriteriaQuery<T> all = cq.select(rootEntry);

        final TypedQuery<T> query = em.createQuery(all);
        return query.getResultList();
    }

    public T findById(Object id) {
        return em.find(entityClass, id);
    }

    @Transactional
    public T save(T t) {
        return em.merge(t);
    }

    @Transactional
    public void remove(T t) {
        em.remove(em.merge(t));
    }

    public List<T> findPaginated(int first, int pageSize, String sortBy, boolean ascending, Map<String, Object> filters) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);

        Root<T> root = criteriaQuery.from(entityClass);

        CriteriaQuery<T> select = criteriaQuery.select(root);

        if(filters != null) {
            filters.forEach((s, o) -> {
                select.where(criteriaBuilder.equal(root.get(s), o));
            });
        }

        if(sortBy != null) {
            select.orderBy(ascending ? criteriaBuilder.asc(root.get(sortBy)) : criteriaBuilder.desc(root.get(sortBy)));
        }

        TypedQuery<T> typedQuery = em.createQuery(select);
        typedQuery.setFirstResult(first);
        typedQuery.setMaxResults(pageSize);

        return typedQuery.getResultList();
    }

    public long count() {
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = qb.createQuery(Long.class);
        cq.select(qb.count(cq.from(entityClass)));

        return em.createQuery(cq).getSingleResult();
    }
}
