package hu.webalapu.webshop.repositories;

import hu.webalapu.webshop.entities.ProductOrder;
import hu.webalapu.webshop.entities.WebShopUser;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import java.util.List;

/**
 * Created by hory9 on 2017.05.05..
 */
@Stateless
public class OrderRepository  extends BaseRepository<ProductOrder>{
    @Inject
    public  OrderRepository(){
        super(ProductOrder.class);
    }

    public ProductOrder findByIdentifier(String identifier) {
        try {
            return (ProductOrder) em.createQuery("SELECT o from ProductOrder o where o.identifier = ?1")
                    .setParameter(1, identifier)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<ProductOrder> findByUser(WebShopUser webShopUser) {
        return em.createQuery("select o from ProductOrder o where o.webShopUser.id = ?1")
                .setParameter(1, webShopUser.getId()).getResultList();
    }
}