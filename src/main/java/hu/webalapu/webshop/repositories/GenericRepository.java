package hu.webalapu.webshop.repositories;

import hu.webalapu.webshop.entities.BaseEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by herperger on 4/30/17.
 */
@Stateless
public class GenericRepository {
    @PersistenceContext
    protected EntityManager em;

    public <T extends BaseEntity> List<T> findAll(Class<T> entityClass) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(entityClass);
        Root<T> rootEntry = cq.from(entityClass);
        CriteriaQuery<T> all = cq.select(rootEntry);

        final TypedQuery<T> query = em.createQuery(all);
        return query.getResultList();
    }

    public <T  extends BaseEntity> T findById(Class<T> entityClass, Object id) {
        return em.find(entityClass, id);
    }

    @Transactional
    public <T extends BaseEntity> T save(T t) {
        return em.merge(t);
    }

    @Transactional
    public <T extends BaseEntity> void remove(T t) {
        em.remove(em.merge(t));
    }

    public <T extends BaseEntity> List<T> findPaginated(Class<T> entityClass, int first, int pageSize, String sortBy, boolean ascending) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);

        Root<T> root = criteriaQuery.from(entityClass);

        CriteriaQuery<T> select = criteriaQuery.select(root);

        if(sortBy != null) {
            select.orderBy(ascending ? criteriaBuilder.asc(root.get(sortBy)) : criteriaBuilder.desc(root.get(sortBy)));
        }

        TypedQuery<T> typedQuery = em.createQuery(select);
        typedQuery.setFirstResult(first);
        typedQuery.setMaxResults(pageSize);

        return typedQuery.getResultList();
    }

    public <T extends BaseEntity> long count(Class<T> entityClass) {
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = qb.createQuery(Long.class);
        cq.select(qb.count(cq.from(entityClass)));

        return em.createQuery(cq).getSingleResult();
    }
}
