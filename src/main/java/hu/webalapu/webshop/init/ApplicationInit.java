package hu.webalapu.webshop.init;

import hu.webalapu.webshop.entities.ProductOrder;
import hu.webalapu.webshop.entities.WebShopUser;
import hu.webalapu.webshop.entities.etc.Address;
import hu.webalapu.webshop.repositories.OrderRepository;
import hu.webalapu.webshop.repositories.UserRepository;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Date;

/**
 * Created by herperger on 3/26/17.
 */

@ApplicationScoped
@Startup
@Singleton
public class ApplicationInit {

    private final UserRepository userRepository;
    private final OrderRepository orderRepository;

    @Inject
    public ApplicationInit(UserRepository userRepository, OrderRepository orderRepository) {
        this.userRepository = userRepository;
        this.orderRepository = orderRepository;
    }


    @PostConstruct
    public void init() {
        createInitialUsers();
    }

    private void createInitialUsers() {
        if(userRepository.findAll().isEmpty()) {
            WebShopUser admin = new WebShopUser();

            admin.username = "admin";
            admin.password = "admin";
            admin.userType = WebShopUser.UserType.ADMIN;
            admin.fullname = "admin admin";
            admin.email = "admin@admin.hu";
            admin.dateOfBirth = new Date(1980,5,5);
            admin.phone = "1234556";
            admin.billingAddress = new Address();
            admin.billingAddress.address = "asd";
            admin.billingAddress.city = "asd";
            admin.billingAddress.country = "asd";

            userRepository.save(admin);

            WebShopUser user1 = new WebShopUser();
            user1.username = "user1";
            user1.password = "asd";
            user1.userType = WebShopUser.UserType.CUSTOMER;
            user1.fullname = "user1 user1";
            user1.email = "user1@webshop.hu";
            user1.dateOfBirth = new Date(1980,5,5);
            user1.phone = "1234556";
            user1.billingAddress.address = "asd2";
            user1.billingAddress.city = "asd";
            user1.billingAddress.country = "asd";

            WebShopUser user2 = new WebShopUser();
            user2.username = "user2";
            user2.password = "asd";
            user2.fullname = "user2 user2";
            user2.email = "user2@webshop.hu";
            user2.dateOfBirth = new Date(1980,5,5);
            user2.phone = "1234556";
            user2.userType = WebShopUser.UserType.CUSTOMER;
            user2.billingAddress.address = "asd3";
            user2.billingAddress.city = "asd";
            user2.billingAddress.country = "asd";

            WebShopUser user3 = new WebShopUser();
            user3.username = "user3";
            user3.password = "asd";
            user3.fullname = "user3 user3";
            user3.email = "user3@webshop.hu";
            user3.dateOfBirth = new Date(1980,5,5);
            user3.phone = "1234556";
            user3.userType = WebShopUser.UserType.CUSTOMER;
            user3.billingAddress.address = "asd4";
            user3.billingAddress.city = "asd";
            user3.billingAddress.country = "asd";

            userRepository.save(user1);
            userRepository.save(user2);
            userRepository.save(user3);
        }
        if(orderRepository.findAll().isEmpty()) {
            ProductOrder order = new ProductOrder();
            order.setWebShopUser(userRepository.findByUsername("user1"));
            order.setOrderStatus(ProductOrder.OrderStatus.NEW);
            orderRepository.save(order);
            ProductOrder order1 = new ProductOrder();
            order1.setWebShopUser(userRepository.findByUsername("user2"));
            order1.setOrderStatus(ProductOrder.OrderStatus.NEW);
            orderRepository.save(order1);
            ProductOrder order2 = new ProductOrder();
            order2.setWebShopUser(userRepository.findByUsername("user3"));
            order2.setOrderStatus(ProductOrder.OrderStatus.NEW);
            orderRepository.save(order2);
        }

    }
}
