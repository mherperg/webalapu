package hu.webalapu.webshop.services;

import hu.webalapu.webshop.entities.*;
import hu.webalapu.webshop.repositories.GenericRepository;
import hu.webalapu.webshop.repositories.ProductInstanceRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by herperger on 5/13/17.
 */
@Stateless
public class OrderService {

    @Inject
    private ProductInstanceRepository productInstanceRepository;

    @Inject
    private GenericRepository genericRepository;

    @Transactional
    public ProductOrder makeOrder(List<CartItem> cart, WebShopUser customer) {
        ProductOrder order = new ProductOrder();

        List<ProductInstance> productInstancesToSave = new ArrayList<>();
        List<OrderItem> orderItemsToSave = new ArrayList<>();

        for (CartItem cartItem: cart) {

            List<ProductInstance> productInstancesByCartItem =
                    productInstanceRepository.findByProduct(cartItem.getProduct(), cartItem.getQuantity());

            if(productInstancesByCartItem.isEmpty()) {
                return null;
            }

            OrderItem orderItem = new OrderItem();
            orderItem.setProduct(cartItem.getProduct());

            productInstancesByCartItem.forEach(productInstance -> {
                orderItem.addProductInstance(productInstance);
                productInstancesToSave.add(productInstance);
            });

            orderItemsToSave.add(orderItem);
        }

        order.setIdentifier(UUID.randomUUID().toString());
        order.setWebShopUser(customer);

        final ProductOrder saved = genericRepository.save(order);

        orderItemsToSave.forEach(orderItem -> {
            orderItem.setOrder(saved);
            genericRepository.save(orderItem);
        });

        productInstancesToSave.forEach(productInstance -> {
            productInstance.setOutOfStock();
            productInstanceRepository.save(productInstance);
        });


        return order;
    }
}
